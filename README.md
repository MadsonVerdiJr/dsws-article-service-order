# DSWS - Ordem de Serviço

- Resumo (somente em português)
- Introdução (apresentar o tema, a motivação e o objetivo)
- Ontologia (apresentar e explicar o modelo ontológico em UML)
- Desenvolvimento
    + Análise (explicar os requisitos da aplicação web)
    + Projeto (explicar a arquitetura da aplicação, ilustrada com diagrama de classe)
    + Implementação (apresentar os frameworks usados e algumas telas da aplicação)
    + Teste (explicar como a aplicação foi testada e apresentar evidências)
- Conclusão (fazer uma reflexão sobre as lições aprendidas)


